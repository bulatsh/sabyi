# -*- coding: utf-8 -*-
import os.path as op
from datetime import datetime
import smtplib
from flask import url_for, redirect, render_template, request, flash
from flask.ext import admin, login
from flask.ext.admin import helpers, expose, Admin, BaseView
from flask.ext.admin.contrib.fileadmin import FileAdmin
from wtforms import form, fields, validators
from wtforms.widgets import TextArea
from flask.ext.admin.contrib.sqla import ModelView
from globs import app, db, manager
from models import Post, Page, User
from werkzeug import generate_password_hash, check_password_hash

class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)

class CKTextAreaField(fields.TextAreaField):
    widget = CKTextAreaWidget()


class AuthModelView(ModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated()

class AuthFileAdmin(FileAdmin):
    def is_accessible(self):
        return login.current_user.is_authenticated()    


class PageAdmin(AuthModelView):
    form_overrides = dict(text=CKTextAreaField)
 
    create_template = 'edit.html'
    edit_template = 'edit.html'


class UserEditForm(form.Form):
    login = fields.TextField(label=u"Логин", validators=[validators.required()])
    current_password = fields.PasswordField(label=u"Текущий пароль", validators=[validators.required()])
    new_password = fields.PasswordField(label=u"Новый пароль")

class UserCreateForm(form.Form):
    login = fields.TextField(label=u"Логин", validators=[validators.required()])
    password = fields.PasswordField(label=u"Пароль", validators=[validators.required()])
    password2 = fields.PasswordField(label=u"Пароль еще раз", validators=[validators.required()])    

class UserAdmin(BaseView):
    @expose("/")
    def index(self):
        objs = db.session.query(User)
        return self.render("profiles.html", objs=objs, title=u"Пользователи")

    @expose('/edit/<login>/', methods=('GET', 'POST'))
    def profile(self, login):
        user = db.session.query(User).filter_by(login=login).first()
        form = UserEditForm(request.form, obj=user)
        if request.method == "POST":
            if helpers.validate_form_on_submit(form):
                data = form.data
                if data.get("new_password") and user.check_password(data["current_password"]):
                    user.set_password(data["new_password"])
                user.login = data["login"]
                db.session.add(user)
                db.session.commit()
                flash("Your profile has been saved")
            else:
                flash("form did not validate on submit")
        return self.render('profile.html', form=form, data=user)

    @expose("/new/", methods=("GET", "POST"))
    def new(self):
        form = UserCreateForm(request.form)
        if request.method == "POST":
            if helpers.validate_form_on_submit(form):
                data = form.data
                if data["password"] == data["password2"]:
                    user = db.session.query(User).filter_by(login=data["login"]).first()
                    if user:
                        flash(u"Пользователь с таким логином уже существует")
                    else:
                        user = User(login=data["login"], 
                            password_hash=generate_password_hash(data["password"]))
                        db.session.add(user)
                        db.session.commit()
                        flash(u"Пользователь создан")
                else:
                    flash(u'Пароли не совпадают')
            else:
                flash("form not valid")
        return self.render("profile.html", form=form)


# Define login and registration forms (for flask-login)
class LoginForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('Invalid user')

        # we're comparing the plaintext pw with the the hash from the db
        if not check_password_hash(user.password_hash, self.password.data):
        # to compare plain text passwords use
        # if user.password != self.password.data:
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(User).filter_by(login=self.login.data).first()


# Initialize flask-login
def init_login():
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)


class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated():
            return redirect(url_for('.login_view'))
        return super(MyAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated():
            return redirect(url_for('.index'))
        link = ''
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))


# Initialize flask-login
init_login()

admin_app = admin.Admin(app, "Admin", index_view=MyAdminIndexView(), base_template='my_master.html')
admin_app.add_view(PageAdmin(Page, db.session, name=u"Страницы"))
admin_app.add_view(UserAdmin(name=u"Пользователи", url="profile"))
admin_app.add_view(PageAdmin(Post, db.session, name=u"Новости"))

path = op.join(op.dirname(__file__), 'static')
admin_app.add_view(AuthFileAdmin(path, '/static/', name=u'Файлы'))

@app.context_processor
def inject_now():
    return dict(now=datetime.now())


@app.route('/')
@app.route('/index.html')
def index():
    return render_template("index.html")

@app.route("/news.html")
def news():
    posts = Post.query.order_by(Post.id.desc()) 
    return render_template("news.html", posts=posts)

@app.route("/<name>.html")
def page(name):
    page = Page.query.filter_by(slug=name).first()
    return render_template("page.html", page=page)

def validate_required():
    return validators.required(message=u"необходимо заполнить")

class ContactForm(form.Form):
    name = fields.TextField(label=u"Имя")
    phone = fields.TextField(label=u"Телефон")
    email = fields.TextField(label=u"Эл. почта", validators=[validate_required()])
    message = fields.TextField(label=u"Сообщение", validators=[validate_required()])

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Поле %s %s" % (
                getattr(form, field).label.text,
                error
            ))

@app.route("/contact", methods=['POST'])
def contact():
    if request.method == "POST":
        form = request.form
        form = ContactForm(request.form)
        page = Page.query.filter_by(slug="contact").first()
        if form.validate():
            message = u"""Имя %s 
            Email %s
            Телефон %s
            Сообщение %s""" % (form.name.data, form.email.data, form.phone.data, form.message.data)
            from email.mime.text import MIMEText
            from subprocess import Popen, PIPE

            msg = MIMEText(message.encode("utf-8"))
            msg["From"] = "website@renal.com"
            msg["To"] = "renalgrp@gmail.com"
            subject = u"Обратная связь. %s %s" % (form.name.data, form.phone.data)
            msg["Subject"] = subject.encode("utf-8")
            p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
            p.communicate(msg.as_string())
            flash(u'Сообщение успешно отправлено. Мы свяжемся с Вами в ближайшее время.')
            return render_template("page.html", page=page)
        flash_errors(form)
        return render_template("page.html", page=page)

if __name__ == '__main__':

    app.debug = True
    app.run(port=8001)

def build_init_db():
    test_user = User(login="admin", password_hash=generate_password_hash("admin"))
    db.session.add(test_user)
    page_slugs = ("index", "about", "news", "contact")
    for slug in page_slugs:
        page = Page(title=slug, slug=slug, text=slug)
        db.session.add(page)
    db.session.commit()