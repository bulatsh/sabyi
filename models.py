from globs import app, db

from werkzeug import generate_password_hash, check_password_hash


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(120))
    images = db.Column(db.UnicodeText, nullable=True)
    date = db.Column(db.Unicode(100))
    text = db.Column(db.UnicodeText, nullable=True)
    
    def get_images(self):
        images = []
        if self.images:
            for line in self.images.split("\n"):
                images.append(line[line.index("/static/"):])
        return images

class Page(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(120))
    slug = db.Column(db.Unicode(100))
    text = db.Column(db.UnicodeText, nullable=True)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100))
    password_hash = db.Column(db.String(200))
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
   
    def check_password(self, password):
            return check_password_hash(self.password_hash, password)

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username